from random import randint

DERNIERCASE = 63


def PartieTerminer(position):
    return position == DERNIERCASE


def PartieTerminer2(position2):
    return position2 == DERNIERCASE


# def SurUneOie(position):
#     return position % 9 == 0


def PositionSuivante(position, de1, de2):
    calculPos = position + de1 + de2
    if calculPos > DERNIERCASE:
        position = DERNIERCASE - (calculPos - DERNIERCASE)
    else:
        position = calculPos
    return position


def PositionSuivante2(position2, de1, de2):
    calculPos2 = position2 + de1 + de2
    if calculPos2 > DERNIERCASE:
        position2 = DERNIERCASE - (calculPos2 - DERNIERCASE)
    else:
        position2 = calculPos2
    return position2


# def CalculOiePosition(position, de1, de2):
#     if position == 9:
#         if de1 == 6 or de2 == 6:
#             print("Vous avez fait 9 par 6 + 3 ---> avancez sur la case 26")
#             return 26
#         else:
#             print("vous avez fait 9 par 5 + 4 ---> avancez sur la case 53")
#             return 53
#     else:
#         print("vous êtes sur une oie avancez de " + str(de1 + de2) + "cases")
#         position = PositionSuivante(position, de1 + de2)
#     return position


def CaseSpecial(position, special, de1, de2):
    if position % 9 == 0 and not PartieTerminer(position) and not position == 54:
        print("Vous êtes sur une oie, vous avancez du double de vos dés")
        return position + (de1 + de2)
    elif position % 5 == 0 and not position == 5 and not position == 10:
        print("Vous êtes sur une OIE NOIRE... Vous avez de la CHANCE... Vous pouvez relancer les dés...")
        de1 = randint(1, 6)
        de2 = randint(1, 6)
        print(f"Vous avez fait : {de1} et {de2}")
        print(f"Malheuresement vous reculez de {de1 + de2} cases...")
        return position - (de1 + de2)
    elif position == 6:
        print("Vous êtes sur le pont traverser jusqu'a la case 12")
        return 12
    elif position == 11:
        print("Vous vous arretez un instant pour vous reposez, et vous admirez la fallaise qui se trouvent a coté de vous")
        print("Vous aimeriez bien l'escaldez pour pouvoir gagner du temps et finir la course au plus vite.")
        print("tout a coup vous tournez la tête de l'autre côté et vous appercevez un kit d'escalade")
        print("grace a lui vous pouvez maintenant escaldez la fallaise, mais attention sa ne sera pas aussi facile")
        print(15 * "-")
        print("Vous commencez a escalader la falaise")
        print("vous arrivez vers le milieu de la falaise quand d'un coup votre pate glisse de la paroi")
        print("Si vous faite 4 avec au moin 1 dé vous arriverez a vous racrocher")
        de1 = randint(1, 6)
        de2 = randint(1, 6)
        print(f"vous avez fait {de1} et {de2}")
        if de1 == 4 or de2 == 4:
            print("Bravo, vous avez fait un 4, vous pouvez continuer votre ascenssion")
            print("Vous êtes presque arrivez tout en haut quand un oiseau vien vous attaquer, une de vos pates vien de lachez")
            print("si vous faite 4 a nouveau vou pourrez finir votre ascenssion mais si vous n'y parvenez pas vous retomberez tout en bas")
            de1 = randint(1, 6)
            de2 = randint(1, 6)
            print(f"vous avez fait {de1} et {de2}")
            if de1 == 4 or de2 == 4:
                print("Bravo, vous faite fuir l'oiseau, vous avez reussi a bravez la falaise, vous êtes desormais sur la case 60")
                return 60
            else:
                print(
                    "Dommage, vous n'êtes pas arrivez a racrochez votre pate, vous chutez mais vous parvenez a vous racrocher au milieu de la falaise"
                )
                print("Votre ascenssion s'arrete la vous êtes desormais sur la case 31 ")
                return 31
        else:
            print("malheuresement, vous n'êtes pas parvenu a vous racrocher, vous tombez... et vous arrivez a nouveau sur la case 11")
            return 11
    elif position == 19:
        special["hotel"] = True
        print("Petit séjour a l'hotel... faites 6 avec au moins 1 dé ")
        de1 = randint(1, 6)
        de2 = randint(1, 6)
        print(f"Vous avez fait : {de1} et {de2}")
        if de1 == 6 or de2 == 6:
            print("Bravo vous pouvez partir de l'hotel")
            special["hotel"] = False
        else:
            print("Dommage, recommencez au prochain tour")
        return 19
    elif position == 26:
        print("vous tomber sur le téléporteur, Vous allez etre téléporter aléatoirement sur le plateau")
        position = randint(1, 63)
        print(f"Vous etes téléporter sur la case {position}")
        return position
    elif position == 31:
        special["puit"] = True
        print("Vous tombez dans le puit... faites 9 avec les 2 dé ")
        de1 = randint(1, 6)
        de2 = randint(1, 6)
        print(f"Vous avez fait : {de1} et {de2}")
        if (
            (de1 == 1 and de2 == 8)
            or (de1 == 2 and de2 == 7)
            or (de1 == 3 and de2 == 6)
            or (de1 == 4 and de2 == 5)
            or (de1 == 5 and de2 == 4)
            or (de1 == 6 and de2 == 3)
            or (de1 == 7 and de2 == 2)
            or (de1 == 8 and de2 == 1)
        ):
            print("Bravo vous avez reussi a sortir du puit")
            special["puit"] = False
        else:
            print("Dommage, recommencez au prochain tour")
        return 31
    elif position == 42:
        print("Vous vous egarez dans le labyrinthe... vous retournez a la case 30 ")
        return 30
    elif position == 52:
        special["prison"] = True
        print("Vous n'avez pas reussi a vous enfuir vous allez en prison...\n" + "Heuresement si vous faite un DOUBLE vous pourez sortir")
        de1 = randint(1, 6)
        de2 = randint(1, 6)
        print(f"Vous avez fait : {de1} et {de2}")
        if (
            (de1 == 1 and de2 == 1)
            or (de1 == 2 and de2 == 2)
            or (de1 == 3 and de2 == 3)
            or (de1 == 4 and de2 == 4)
            or (de1 == 5 and de2 == 5)
            or (de1 == 6 and de2 == 6)
        ):
            print("vous avez fait un double, vous pouvez sortir !")
            special["prison"] = False
        else:
            print("Dommage, recommencez au prochain tour")
        return 52
    elif position == 58:
        print("Un requin est apparu... Vous êtes mort... vous retournez a la case 1...")
        return 1
    else:
        print(f"la case {position} a l'air sûr")
        return position


def CaseSpecial2(position2, special2, de1, de2):
    if position2 % 9 == 0 and not PartieTerminer2(position2) and not position2 == 54:
        print("Vous êtes sur une oie, vous avancez du double de vos dés")
        print(f"vous allez sur la case {position2 + (de1 + de2)}")
        return position2 + (de1 + de2)
    elif position2 % 5 == 0 and not position2 == 5 and not position2 == 10:
        print("Vous êtes sur une OIE NOIRE... Vous avez de la CHANCE... Vous pouvez relancer les dés...")
        de1 = randint(1, 6)
        de2 = randint(1, 6)
        print(f"Vous avez fait : {de1} et {de2}")
        print(f"Malheuresement vous reculez de {de1 + de2} cases...")
        return position2 - (de1 + de2)
    elif position2 == 6:
        print("Vous êtes sur le pont traverser jusqu'a la case 12")
        return 12
    elif position2 == 11:
        print("Vous vous arretez un instant pour vous reposez, et vous admirez la fallaise qui se trouvent a coté de vous")
        print("Vous aimeriez bien l'escaldez pour pouvoir gagner du temps et finir la course au plus vite.")
        print("tout a coup vous tournez la tête de l'autre côté et vous appercevez un kit d'escalade")
        print("grace a lui vous pouvez maintenant escaldez la fallaise, mais attention sa ne sera pas aussi facile")
        print(15 * "-")
        print("Vous commencez a escalader la falaise")
        print("vous arrivez vers le milieu de la falaise quand d'un coup votre pate glisse de la paroi")
        print("Si vous faite 4 avec au moin 1 dé vous arrivez a vous racrocher")
        de1 = randint(1, 6)
        de2 = randint(1, 6)
        print(f"vous avez fait {de1} et {de2}")
        if de1 == 4 or de2 == 4:
            print("Bravo, vous avez fait un 4, vous pouvez continuer votre ascenssion")
            print("Vous êtes presque arrivez tout en haut quand un oiseau vien vous attaquer, une de vos pates vien de lachez")
            print("si vous faite 4 a nouveau vou pourrez finir votre ascenssion mais si vous n'y parvenez pas vous retomberez tout en bas")
            de1 = randint(1, 6)
            de2 = randint(1, 6)
            print(f"vous avez fait {de1} et {de2}")
            if de1 == 4 or de2 == 4:
                print("Bravo, vous faite fuir l'oiseau, vous avez reussi a bravez la falaise, vous êtes desormais sur la case 60")
                return 60
            else:
                print(
                    "Dommage, vous n'êtes pas arrivez a racrochez votre pate, vous chutez mais vous parvenez a vous racrocher au milieu de la falaise"
                )
                print("Votre ascenssion s'arrete la vous êtes desormais sur la case 31 ")
                return 31
        else:
            print("malheuresement, vous n'êtes pas parvenu a vous racrocher, vous tombez... et vous arrivez a nouveau sur la case 11")
            return 11
    elif position2 == 19:
        special2["hotel2"] = True
        print("Petit séjour a l'hotel... faites 6 avec au moins 1 dé ")
        de1 = randint(1, 6)
        de2 = randint(1, 6)
        print(f"Vous avez fait : {de1} et {de2}")
        if de1 == 6 or de2 == 6:
            print("Bravo vous pouvez partir de l'hotel")
            special2["hotel2"] = False
        else:
            print("Dommage, recommencez au prochain tour")
        return 19
    elif position2 == 26:  # téléporteur
        print("vous tomber sur le téléporteur, Vous allez etre téléporter aléatoirement sur le plateau")
        position2 = randint(1, 63)
        print(f"Vous etes téléporter sur la case {position2}")
        return position2
    elif position2 == 31:
        special2["puit2"] = True
        print("Vous tombez dans le puit... faites 9 avec les 2 dés ")
        de1 = randint(1, 6)
        de2 = randint(1, 6)
        print(f"Vous avez fait : {de1} et {de2}")
        if (
            (de1 == 1 and de2 == 8)
            or (de1 == 2 and de2 == 7)
            or (de1 == 3 and de2 == 6)
            or (de1 == 4 and de2 == 5)
            or (de1 == 5 and de2 == 4)
            or (de1 == 6 and de2 == 3)
            or (de1 == 7 and de2 == 2)
            or (de1 == 8 and de2 == 1)
        ):
            print("Bravo vous avez reussi a sortir du puit")
            special2["puit2"] = False
        else:
            print("Dommage, recommencez au prochain tour")
        return 31
    elif position2 == 42:
        print("Vous vous egarez dans le labyrinthe... vous retournez a la case 30 ")
        return 30
    elif position2 == 52:
        special2["prison2"] = True
        print("Vous n'avez pas reussi a vous enfuir vous allez en prison...\n" + "Heuresement si vous faite un DOUBLE vous pourez sortir")
        de1 = randint(1, 6)
        de2 = randint(1, 6)
        print(f"Vous avez fait : {de1} et {de2}")
        if (
            (de1 == 1 and de2 == 1)
            or (de1 == 2 and de2 == 2)
            or (de1 == 3 and de2 == 3)
            or (de1 == 4 and de2 == 4)
            or (de1 == 5 and de2 == 5)
            or (de1 == 6 and de2 == 6)
        ):
            print("Vous avez fait un double vous pouvez partir")
            special2["prison2"] = False
        else:
            print("Dommage, recommencez au prochain tour")
        return 52
    elif position2 == 58:
        print("Vous êtes mort... vous retournez a la case 1...")
        return 1
    else:
        print(f"la case {position2} a l'air sûr")
        return position2


def JeuDeLoie():
    position = 0
    position2 = 0
    special = {"puit": False, "prison": False, "hotel": False}
    special2 = {"puit2": False, "prison2": False, "hotel2": False}
    joueur1 = True
    joueur2 = True
    while not PartieTerminer(position) and not PartieTerminer2(position2):
        if joueur1 is True:
            print("--" * 30)
            print("Tour du joueur 1")
            de1 = randint(1, 6)
            de2 = randint(1, 6)
            # de1 = 5
            # de2 = 0

            if special["puit"]:
                if (
                    (de1 == 1 and de2 == 8)
                    or (de1 == 2 and de2 == 7)
                    or (de1 == 3 and de2 == 6)
                    or (de1 == 4 and de2 == 5)
                    or (de1 == 5 and de2 == 4)
                    or (de1 == 6 and de2 == 3)
                    or (de1 == 7 and de2 == 2)
                    or (de1 == 8 and de2 == 1)
                ):
                    special["puit"] = False
                    print(f"vous avez fait {de1} + {de2} = {de1+de2}")
                    print("Bravo, Vous arrivez a sortir du puit")
                else:
                    print(f"Dommage, vous avez fait {de1} et {de2}")
                    print("recommencer au prochain tour")
                    joueur1 = False
                    joueur2 = True
            elif special["prison"]:
                if (
                    (de1 == 1 and de2 == 1)
                    or (de1 == 2 and de2 == 2)
                    or (de1 == 3 and de2 == 3)
                    or (de1 == 4 and de2 == 4)
                    or (de1 == 5 and de2 == 5)
                    or (de1 == 6 and de2 == 6)
                ):
                    special["prison"] = False
                else:
                    print(f"Dommage, vous avez fait {de1} et {de2}")
                    print("recommencer au prochain tour")
                    joueur1 = False
                    joueur2 = True
            elif special["hotel"]:
                if de1 == 6 or de2 == 6:
                    special["hotel"] = False
                    print("Vous avez fait 6 ! Votre séjour à l'hotel prend fin")
                else:
                    print(f"Dommage, vous avez fait {de1} et {de2}")
                    print("recommencer au prochain tour")
                    joueur1 = False
                    joueur2 = True
            else:
                print(f"vous êtes sur la case {position}")
                print(f"lancer de des ==> {de1} + {de2} = {de1 + de2}")
                position = PositionSuivante(position, de1, de2)
                print(f"vous aller sur la case {position}")
                position = CaseSpecial(position, special, de1, de2)
                joueur1 = False
                joueur2 = True
                if position == DERNIERCASE:
                    print("BRAVO, Vous avez gagné")
            input()
        elif joueur2 is True:
            print("--" * 30)
            print("Tour du joueur 2")
            de1 = randint(1, 6)
            de2 = randint(1, 6)
            # de1 = 5
            # de2 = 0

            if special2["puit2"]:
                if (
                    (de1 == 1 and de2 == 8)
                    or (de1 == 2 and de2 == 7)
                    or (de1 == 3 and de2 == 6)
                    or (de1 == 4 and de2 == 5)
                    or (de1 == 5 and de2 == 4)
                    or (de1 == 6 and de2 == 3)
                    or (de1 == 7 and de2 == 2)
                    or (de1 == 8 and de2 == 1)
                ):
                    special2["puit2"] = False
                    print(f"vous avez fait {de1} + {de2} = {de1+de2}")
                    print("Bravo, Vous arrivez a sortir du puit")
                else:
                    print(f"Dommage, vous avez fait {de1} et {de2}")
                    print("recommencer au prochain tour")
                    joueur1 = True
                    joueur2 = False
            elif special2["prison2"]:
                if (
                    (de1 == 1 and de2 == 1)
                    or (de1 == 2 and de2 == 2)
                    or (de1 == 3 and de2 == 3)
                    or (de1 == 4 and de2 == 4)
                    or (de1 == 5 and de2 == 5)
                    or (de1 == 6 and de2 == 6)
                ):
                    special2["prison2"] = False
                else:
                    print(f"Dommage, vous avez fait {de1} et {de2}")
                    print("recommencer au prochain tour")
                    joueur1 = True
                    joueur2 = False
            elif special2["hotel2"]:
                if de1 == 6 or de2 == 6:
                    special2["hotel2"] = False
                    print("Vous avez fait 6 ! Votre séjour à l'hotel prend fin")
                else:
                    print(f"Dommage, vous avez fait {de1} et {de2}")
                    print("recommencer au prochain tour")
                    joueur1 = True
                    joueur2 = False
            else:
                print(f"vous êtes sur la case {position2}")
                print(f"lancer de des ==> {de1} + {de2} = {de1 + de2}")
                position2 = PositionSuivante2(position2, de1, de2)
                print(f"vous aller sur la case {position2}")
                position2 = CaseSpecial2(position2, special2, de1, de2)
                joueur1 = True
                joueur2 = False
                if position2 == DERNIERCASE:
                    print("BRAVO, Vous avez gagné")
            input()


JeuDeLoie()
